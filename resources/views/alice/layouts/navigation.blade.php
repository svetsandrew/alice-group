@if($menu)
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mx-auto">
            @foreach($menu->items as $item)
                <li class="nav-item {{ (URL::current() == $item->url()) ? 'active' : '' }}">
                    <a itemprop="url" class="nav-link" href="{{$item->url() }}">{{ $item->title }}</a>
                </li>
            @endforeach
        </ul>
        <div class="header-mail-phone">
            <a class="phone" href="tel:+74959027723">+7 (495) 902-77-23</a>
            <a href="mailto:info@alicegroup.ru">info@alicegroup.ru</a>
        </div>
    </div>
@endif
