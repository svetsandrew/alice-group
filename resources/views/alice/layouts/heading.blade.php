<div class="container">
    <div class="heading py-5">
        <h2>{{ $h_title }}</h2>
        <div class="lead">
            {!! $page_desc !!}
        </div>
    </div>
</div>