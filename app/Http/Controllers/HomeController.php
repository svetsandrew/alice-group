<?php

namespace Alice\Http\Controllers;

use Alice\Repositories\ServicesRepository;
use Illuminate\Http\Request;
use Alice\Page;

class HomeController extends SiteController
{

    public function __construct(){
        parent::__construct(new \Alice\Repositories\MenuRepository(new \Alice\Menu));

        $this->side = true;
        $this->template = env('THEME').'.home';
    }

    /**
     * Output home page
     * @return $this
     * @throws \Throwable
     */
    public function index() {
        $this->sideTitle = 'Создадим книгу из рукописи';

        $home = Page::where('params','home')->first();

        $content = view(env('THEME').'.layouts.homeContent')->with('sideTitle', $this->sideTitle)->with('home', $home)->render();
        $this->vars = array_add($this->vars,'content', $content);
        $this->vars = array_add($this->vars,'title', $home->title);
        $this->vars = array_add($this->vars,'meta_keywords', $home->meta_keywords);
        $this->vars = array_add($this->vars,'meta_desc', $home->meta_description);

        return $this->renderOutput();
    }
}
