<?php

namespace Alice;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model {
    protected $table = "delivery";
}
