<?php

namespace Faker\Provider\el_GR;

class Text extends \Faker\Provider\Text
{
    /**
     * From el.wikisource.org.
     *
     * The text is licensed under the Creative Commons Attribution / Share-Alike,
     * Also additional terms may apply. For details, see. Terms of Use.
     *
     *
     * Title: Τρελαντώνης
     *
     * Author: Πηνελόπη Δέλτα
     *
     * Posting Date: January 6, 2016
     * Release Date: 1932
     * [Last updated: September 1, 2013]
     *
     * Language: Greek
     *
     * @licence Creative Commons Attribution-ShareAlike https://creativecommons.org/licenses/by-sa/3.0/deed.el
     *
     * @see     https://wikimediafoundation.org/wiki/Terms_of_Use/
     * @link    https://el.wikisource.org/wiki/%CE%A4%CF%81%CE%B5%CE%BB%CE%B1%CE%BD%CF%84%CF%8E%CE%BD%CE%B7%CF%82
     *
     * @var string
     */
    protected static $baseText = <<<'EOT'
Ο Αντώνης ήταν πολύ σκάνταλος και πολύ άτακτος και κάθε λίγο έβρισκε τον μπελά του. Δεν περνούσε μέρα που να μην έτρωγε δυο τρεις κατσάδες, πότε από τη θεία του, πότε από τη μαγείρισσα, πότε από την Αγγλίδα δασκάλα και πότε από την τραπεζιέρα, και κάθε λίγο αναγκάζουνταν ν' ανακατώνεται ο θείος. Σαν έφθανε απέξω ο θείος και άκουε την καινούρια αταξία του Αντώνη, το αγαθό του πρόσωπο αγρίευε όσο μπορούσε, σούρωνε τ' άσπρα του φρύδια και, κουνώντας το σταχτί του κεφάλι, έλεγε αυστηρά:
- Αντώνη, ακούω πάλι πως έκανες αταξίες! Φοβούμαι πως δε θα τα πάμε καλά!
Αυτές ήταν οι σοβαρές περιστάσεις. Άκουε η Αλεξάνδρα, η μεγάλη αδελφή, και ντρέπουνταν για τον αδελφό της. Άκουε η Πουλουδιά, η μικρότερη αδελφή, κι ένιωθε την καρδιά της να παίζει τούμπανο. Άκουε και ο μικρός ο Αλέξανδρος, καθισμένος στο πάτωμα, με το δάχτυλο στο στόμα, και αποφάσιζε μέσα του πως εκείνος δεν ήθελε να γίνει έτσι κακό παιδί σαν τον Αντώνη.
Και όμως πώς ήθελε να μπορεί να κάνει όσα έκανε ο Αντώνης! Γιατί ο Αντώνης έκανε πολλά δύσκολα πράματα. Έκανε τούμπες τρεις στη σειρά και θα έκανε, λέει, και τέσσερις, αν ήταν πιο μεγάλη η κάμαρα και αν δε χτυπούσε ο τοίχος στα ποδάρια του· σκαρφάλωνε στη γαζία της αυλής· καβαλίκευε στην κουπαστή της σκάλας και κατέβαινε γλιστρώντας ως κάτω - έκανε, πηδώντας με το ένα πόδι, τρεις φορές το γύρο της αυλής του σπιτιού, χωρίς ν' αγγίξει τον τοίχο - κάθε πρωί, στη θάλασσα, βουτούσε το κεφάλι του στο νερό κι έμενε τόση ώρα με κλειστό στόμα και ανοιχτά μάτια, και δεν πνίγουνταν ποτέ. Και' άλλα πολλά έκανε ο Αντώνης. Έπειτα είχε πάντα γεμάτες τις τσέπες του από τόσους θησαυρούς. Τι δεν έβρισκες μέσα! Καρφιά, βόλους, βότσαλα, σπάγκους, κάποτε και κανένα κομμάτι μαστίχα μασημένη, και, πάνω απ' όλα, το τρίγωνο γυαλί που είχε πέσει από τον πολυέλαιο της εκκλησίας και που έκανε τόσα ωραία χρώματα σαν το έβαζες στον ήλιο. Ολόκληρο πλούτο είχαν αυτές οι τσέπες του Αντώνη.
Οι γονείς του Αντώνη, που ζούσαν στην Αίγυπτο, δεν μπόρεσαν να ταξιδέψουν εκείνο το καλοκαίρι, κι εκείνος και τ' αδέλφια του είχαν έλθει στον Πειραιά με το θείο Ζωρζή και τη θεία Μαριέτα, που δεν είχαν παιδιά, και κάθουνταν σ' ένα από τα σπίτια του Τσίλερ. Επτά ήταν τα σπίτια του Τσίλερ, όλα στην αράδα κι ενωμένα· το πρώτο, το ακριανό, μεγάλο, με τρία πρόσωπα, τ' άλλα όλα όμοια, με μια βεραντούλα προς τη θάλασσα και μιαν αυλή στο πίσω μέρος, προς το λόφο.
Στο πρώτο, το μεγάλο σπίτι, κάθουνταν ο βασιλέας· στο δεύτερο μια Ρωσίδα, κυρία της Τιμής της βασίλισσας· στο τρίτο ο Αντώνης με τ' αδέλφια του και το θείο του και τη θεία, και στ' άλλα παρακάτω διάφοροι άλλοι που, σαν το βασιλέα, είχαν κατέβει από τας Αθήνας να περάσουν τους ζεστούς μήνες του καλοκαιριού κοντά στην πειραιώτικη θάλασσα.
Κάθε μέρα ο Αντώνης και τ' αδέλφια του πήγαιναν περίπατο με την Εγγλέζα τους δασκάλα και περνούσαν εμπρός στο μεγάλο σπίτι όπου κάθουνταν ο βασιλέας, που είχε μεγάλα σκυλιά του κυνηγιού. Τ' άκουε ο Αντώνης που γάβγιζαν και τραβούσαν τις αλυσίδες τους, κλεισμένα στην αυλή τους την περιτοιχισμένη, και κάθε φορά ο κρότος αυτός και τα γαβγίσματα ήταν μεγάλος πειρασμός. Και τα τέσσερα αδέλφια γνώριζαν καλά τα σκυλιά αυτά και ιδιαίτερα ένα, τον Ντον. Τα έβλεπαν συχνά με το βασιλέα, που τα έπαιρνε μαζί του, λυτά, ελεύθερα, κάθε φορά που έβγαινε περίπατο μονάχος.
Ήταν μεγάλος πειρασμός για τον Αντώνη τα σκυλιά αυτά και κάθε φορά που περνούσε μπρος στο σπίτι του βασιλέα με την Εγγλέζα δασκάλα του, έμενε πίσω, έκανε ελιγμούς, έβρισκε διάφορες προφάσεις για να πλησιάσει την πόρτα της αυλής, μήπως και τύχει να είναι μισάνοιχτη ή μήπως και βρει καμιά χαραματιά που να τον αφήσει να δει τον Ντον, το μεγάλο κανελί σκυλί με τα παράταιρα μάτια, το ένα γαλάζιο και το άλλο πράσινο. Μα πού να ξεφύγει από το βλέμμα της Εγγλέζας! Ξερή και μονοκόμματη γύριζε αυτή, τη στιγμή που νόμιζε κείνος πως είχε γλιτώσει, τον κεραυνοβολούσε με μια ματιά και τον συμμάζευε, κατσουφιασμένο μα δαμασμένο, στο μπουλούκι των τριών πιο φρόνιμων.
- Είναι κακιά και γρουσούζα... μουρμούριζε ο Αντώνης στις αδελφές του, καμτσικώνοντας τις πέτρες του δρόμου με κανένα μαδημένο από τα φύλλα του χλωρό κλαδί, που πάντα βρίσκουνταν ανάμεσα στους θησαυρούς του Αντώνη, προς μεγάλο θαυμασμό του Αλέξανδρου. Είναι τσίφνα και γρινιάρα...
- Τι είναι; ρωτούσε ο Αλέξανδρος γέρνοντας ολόκληρος εμπρός από τη δασκάλα, που τον βαστούσε σφιχτά από το χέρι, για ν' ακούσει τη λέξη που του ξέφυγε. Μ' αμέσως τον τίναζε πίσω η Εγγλέζα, που δεν καταλάβαινε τα ελληνικά, και τον ξανάφερνε στη θέση του πλάγι της.
- Σπίκ Ίνγκλις! πρόσταζε με το πιο αυστηρό της ύφος!
Και μαζεμένα πάλι, την ακολουθούσαν τα τέσσερα αδέλφια, με ίσιες τις ράχες και σφιγμένα τα χείλια, παρατώντας κάθε αρχισμένη κουβέντα, για να της δείξουν την αποδοκιμασία τους. Κι έτσι, σιωπηλά, έκαναν το γύρο του βράχου, ανέβαιναν στο λόφο, απομακρύνουνταν από τον περαστικό δρόμο. Κι εκεί στη μοναξιά, στις πέτρες και στα ξερά χαμόκλαρα, κάθουνταν όλα τ' αδέλφια στην αράδα, με τα πόδια ενωμένα και τα χέρια σταυρωμένα φρόνιμα μπροστά τους, και κοίταζαν από πάνω, ψηλά, τις βαρκούλες που αρμένιζαν μακριά στο πέλαγος και πιο κοντά, βαθιά, κάτω, στους βράχους, τις πέτρες που ξεχώριζαν μια μια στα διάφανα βαθυγάλαζα νερά της Καστέλας.
Σε λίγο σηκώνουνταν η δασκάλα, έκανε πως συγυρίζει τα φορέματα της κι έβγαζε κρυφά κάτι από την τσέπη της. Ύστερα άνοιγε το βιβλίο της και κάθουνταν, γυρίζοντας τη ράχη της στα τέσσερα αδέλφια.
Έκανε πως διάβαζε. Μα τ' αδέλφια ήξεραν πως δε διάβαζε καθόλου. Γιατί ο Αντώνης την είχε δει δυο φορές που κρυφά έβαζε στα χείλια της μια μποτίλια με κάτι κανελί μέσα και το έπινε και πάλι βιαστικά το έκρυβε κάτω από τους φραμπαλάδες της φούστας της. Τότε άρχιζε η καλή ώρα του Αντώνη και των αδελφών του. Ό,τι ήθελαν έκαναν. Η δασκάλα δεν τους κοίταζε πια. Ο Αντώνης έδινε το σύνθημα κι ένας ένας σηκώνουνταν σιωπηλά και απομακρύνουνταν.
Και τότε γίνουνταν το ανάστα ο Θεός. Έτρεχαν, πηδούσαν, κατέβαιναν στο δρόμο, σκαρφάλωναν στους βράχους, έπεφταν, σηκώνουνταν, φώναζαν, δέρνουνταν, καβγάδιζαν ή γελούσαν, βουτούσαν στις σκόνες, έπιαναν ακρίδες, μάζευαν βότσαλα, πετούσαν πέτρες, τίποτε πια δεν έβλεπε ούτε άκουε η δασκάλα. Χωμένη στο βιβλίο της, ρουφώντας κρυφά την μποτίλια της, άφηνε τ' αδέλφια ελεύθερα.
Και τι ωραία που ήταν η ελευθερία στο βράχο της Καστέλας! Πουθενά δεν ήταν τόσο ψιλή η σκόνη, τα χαμόκλαδα πιο ξερά, τα κλαριά πιο εύκολα να τσακίσουν, οι πέτρες πιο πολλές, το χώμα πιο πλούσιο από θησαυρούς.
Τι δεν έβρισκες εκεί μέσα! Πράσινα και γαλάζια κομμάτια γυαλί, κάποτε και άσπρα, χαλκάδες τενεκεδένιους σκουριασμένους ή καπάκια κουτιών στρογγυλά, σα ρόδες χωρίς αξόνι - μα ο Αντώνης έλεγε πως ήταν εύκολο να τους κάνεις αξόνι μ' ένα καρφί που θα τα τρυπούσε στη μέση - κάποτε κανένα κουδουνάκι σιδερένιο χωρίς γλωσσίδι - που και αυτό διορθώνουνταν, βεβαίωνε ο Αντώνης, με μια μεγάλη χάντρα της Αλεξάνδρας, κρεμασμένη σε μια κλωστή, μόνο που η Αλεξάνδρα, που είχε πέντε τέτοιες χάντρες, δεν ήθελε να δώσει καμιά, - κάποτε κανένα κομμάτι σκοινί ή σπάγκο ή τέλι, μα προπάντων πέτρες, πέτρες όλων των σχημάτων, με φλέβες σταχτιές, μενεξελιές, τριανταφυλλιές ή μαύρες.
Μια μέρα, η Πουλουδιά βρήκε έναν αληθινό θησαυρό· βόλους, βόλους μαύρους, πολλούς, σκόρπιους, μικρούς, ολοστρόγγυλους. Ζαλισμένη τους κοίταζε, άφωνη από τη χαρά της. Η πρώτη της σκέψη ήταν να μη φωνάξει τ' αδέλφια της, μην της τους πάρει ο Αντώνης, που πρέσβευε πως τα κορίτσια δεν πρέπει να παίζουν βόλους, πως έχουν κούκλες και πως αυτές τους αρκούν. Μα ήταν τόσο πολλοί οι σκόρπιοι βόλοι, αρκούσαν για όλους, ακόμα και για τον Αλέξανδρο, που τόσο τους λαχταρούσε και που ποτέ δεν του δάνειζε τους δικούς του ο Αντώνης.
Φώναξε λοιπόν τ' αδέλφια της.
- Βόλους! Βόλους! Ελάτε να δείτε πόσοι! τους είπε μαζεύοντας τους στη γεμάτη φούχτα της.
Ανακούρκουδα πλάγι της, καταχαρούμενος, τους μάζευε ο Αλέξανδρος έναν έναν και τους φύλαγε στην άλλη παλάμη του. Ο Αντώνης όμως, με τα δυο του χέρια στις τσέπες του πανταλονιού του, δεν το θεώρησε αξιόπρεπο για ένα αγόρι να ενθουσιαστεί με το εύρημα ενός κοριτσιού. Και είπε ακατάδεχτα:
- Οι δικοί μου είναι πιο μεγάλοι!
Και η Αλεξάνδρα, που ακολουθούσε πάντα τον Αντώνη, είπε:
- Και για να τους πετάξουν εδώ, θα πει πως είναι χαλασμένοι!
Απογοητευμένος άνοιξε ο Αλέξανδρος τα χέρια του και οι βόλοι του σκορπίστηκαν στο χώμα. Η Πουλουδιά όμως επέμεινε.
- Πού το ξέρεις πως τους πέταξαν; ρώτησε. Μπορεί ένα αγόρι να τους είχε στην τσέπη του και να τρύπησε η τσέπη του και να του έπεσαν όσο περπατούσε. Για δες, έχει παντού, εδώ, κι εκεί, και παρακάτω! Θα έτρεχε το αγόρι και θα έπεφταν οι βόλοι...
- Πφφφ... διέκοψε ο Αντώνης που είχε πολλή όρεξη να πάρει από το θησαυρό της αδελφής του, μα που δεν το καταδέχουνταν πια, μιας και τον είχε περιγελάσει. Ξέρεις και συ τώρα από αγόρια!
Πειραγμένη στο φιλότιμο της για την περιφρονητική αδιαφορία των αδελφών της, η Πουλουδιά γέμισε την τσέπη της και είπε:
- Καλά. Όταν αύριο μου ζητήσετε τους βόλους μου, εγώ δε θα σας τους δώσω!
Και κάκιωσε και δεν ήθελε να ρίξει με τους άλλους πέτρες στο γιαλό.
Αυτό ήταν το πιο ωραίο τους παιχνίδι. Και παράμερα, λυπημένη, κοίταζε η Πουλουδιά τις πέτρες που τις πετούσαν τ' αδέλφια της και που πηδούσαν στους βράχους και ξαναπηδούσαν ως κάτω κι έπεφταν πλουφ στο νερό.
Το πλουφ δεν το άκουαν, γιατί ήταν πολύ ψηλά και το νερό πολύ χαμηλά. Μα τους είχε πει ο Αντώνης πως κάνει πάντα πλουφ η πέτρα στο νερό, και αν το 'λεγε ο Αντώνης, πρέπει να ήταν αλήθεια. Γιατί ποτέ δεν είπε ψέμα ο Αντώνης.
Και το 'λεγε πάντα ο θείος: «Ο Αντώνης είναι σκάνταλος, μα ψέματα δε λέγει!» Και το 'λεγε και η θεία, που του τις έβρεχε συχνά. Και το 'λεγε η κερα-Ρήνη η μαγείρισσα, που, σαν είχε πονοκέφαλο, έδενε ένα μαντίλι γύρω στο μέτωπο της με φέτες πατάτες ή λεμόνια στα μηνίγγια της κι έβριζε μες στα δόντια της κι έλεγε: «Ντελή- Αντώνης, Τρελαντώνης, που κακό να μην τον πιάσει... γιατί ψέματα δεν ξέρει!»
Το 'λεγε και η Αφροδίτη η τραπεζιέρα, που ήταν καλή, όσο ήταν άσχημη, που γύρευε να κρύψει από τη θεία τις ζημιές του, μα που τις ομολογούσε ύστερα εκείνος. Όλα τα ομολογούσε, σαν τον ρωτούσαν. Και τον έτρεμαν τα κορίτσια, μην ομολογήσει και τις δικές τους αταξίες και τα δικά τους μυστικά.
Έτσι, εκείνη την ημέρα που βρήκε το θησαυρό της, πέρασε η Πουλουδιά μεγάλη στενοχώρια.
Είχαν επιστρέψει τ' αδέλφια στο σπίτι με την Εγγλέζα δασκάλα που αισθάνθηκε, λέει, κακοδιάθετη και ανέβηκε στην κάμαρα της. Τ' αδέλφια μείναν στην αυλή για να παίξουν, που ήταν ακόμα μέρα, μα κανένας δεν είχε κέφι για παιχνίδια, γιατί ο Αλέξανδρος, στην επιστροφή εκείνο το απόγεμα