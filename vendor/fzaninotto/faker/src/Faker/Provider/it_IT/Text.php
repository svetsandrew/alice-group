<?php

namespace Faker\Provider\it_IT;

class Text extends \Faker\Provider\Text
{
    /**
     * Carlo Collodi
     * Pinocchio
     * Questo e-book è stato realizzato anche grazie al sostegno di:
     *
     * E-text
     * Web design, Editoria, Multimedia
     * (pubblica il tuo libro, o crea il tuo sito con E-text!)
     * http://www.e-text.it/
     *
     * QUESTO E-BOOK:
     *
     * TITOLO: Pinocchio
     * AUTORE: Collodi, Carlo
     * TRADUTTORE:
     * CURATORE:
     * NOTE:
     *
     * CODICE ISBN E-BOOK: 9788890359767
     *
     * DIRITTI D'AUTORE: no
     *
     * LICENZA: questo testo è distribuito con la licenza specificata al seguente indirizzo Internet: http://www.liberliber.it/libri/licenze/ (Attribution-NonCommercial-ShareAlike 4.0 International)
     *
     * TRATTO DA: Le avventure di Pinocchio : storia di un burattino / di Carlo Collodi ; illustrata da Enrico Mazzanti. – 2a edizione. – Milano : Rizzoli Editore, Milano, 1949.
     *
     * CODICE ISBN: informazione non disponibile
     *
     * 1a EDIZIONE ELETTRONICA DEL: 28 gennaio 1996
     * 2a EDIZIONE ELETTRONICA DEL: 17 gennaio 2002
     * 3a EDIZIONE ELETTRONICA DEL: 19 luglio 2013
     *
     * INDICE DI AFFIDABILITA': 1
     * 0: affidabilità bassa
     * 1: affidabilità media
     * 2: affidabilità buona
     * 3: affidabilità ottima
     *
     * DIGITALIZZAZIONE:
     * Riccardo Scateni
     *
     * REVISIONE:
     * Marco Zela
     *
     * IMPAGINAZIONE:
     * Riccardo Scateni
     * Marco Zela
     * Catia Righi
     *
     * PUBBLICAZIONE:
     * Marco Calvo, http://www.marcocalvo.it/
     * Informazioni sul "progetto Manuzio"
     * Il "progetto Manuzio" è una iniziativa dell'associazione culturale Liber Liber. Aperto a chiunque voglia collaborare, si pone come scopo la pubblicazione e la diffusione gratuita di opere letterarie in formato elettronico. Ulteriori informazioni sono disponibili sul sito Internet:
     * http://www.liberliber.it/
     * Aiuta anche tu il "progetto Manuzio"
     * Se questo "libro elettronico" è stato di tuo gradimento, o se condividi le finalità del "progetto Manuzio", invia una donazione a Liber Liber. Il tuo sostegno ci aiuterà a far crescere ulteriormente la nostra biblioteca. Qui le istruzioni:
     * http://www.liberliber.it/aiuta/
     *
     * Indice generale
     * I. Come andò che maestro Ciliegia, falegname, trovò un pezzo di legno, che piangeva e rideva come un bambino.
     * II. Maestro Ciliegia regala il pezzo di legno al suo amico Geppetto, il quale lo prende per fabbricarsi un burattino maraviglioso che sappia ballare, tirar di scherma e fare i salti mortali.
     * III Geppetto, tornato a casa, comincia subito a fabbricarsi il burattino e gli mette il nome di Pinocchio. Prime monellerie del burattino.
     * IV La storia di Pinocchio col Grillo-parlante, dove si vede come i ragazzi cattivi hanno a noia di sentirsi correggere da chi ne sa più di loro.
     * V Pinocchio ha fame, e cerca un uovo per farsi una frittata; ma sul più bello, la frittata gli vola via dalla finestra.
     * VI Pinocchio si addormenta coi piedi sul caldano, e la mattina dopo si sveglia coi piedi tutti bruciati.
     * VII Geppetto torna a casa, e dà al burattino la colazione che il pover’uomo aveva portata con sé.
     * VIII Geppetto rifà i piedi a Pinocchio e vende la propria casacca per comprargli l’Abbecedario.
     * IX Pinocchio vende l’Abbecedario per andare a vedere il teatrino dei burattini.
     * X I burattini riconoscono il loro fratello Pinocchio e gli fanno una grandissima festa; ma sul più bello, esce fuori il burattinaio Mangiafoco, e Pinocchio corre il pericolo di fare una brutta fine.
     * XI Mangiafoco starnutisce e perdona a Pinocchio, il quale poi difende dalla morte il suo amico Arlecchino.
     * XII Il burattinaio Mangiafoco regala cinque monete d’oro a Pinocchio, perché le porti al suo babbo Geppetto: e Pinocchio, invece, si lascia abbindolare dalla Volpe e dal Gatto e se ne va con loro.
     * XIII L’osteria del Gambero Rosso.
     * XIV Pinocchio, per non aver dato retta ai buoni consigli del Grillo-parlante, s’imbatte negli assassini.
     * XV Gli assassini inseguono Pinocchio; e, dopo averlo raggiunto, lo impiccano a un ramo della Quercia grande.
     * XVI La bella Bambina dai capelli turchini fa raccogliere il burattino: lo mette a letto, e chiama tre medici per sapere se sia vivo o morto.
     * XVII Pinocchio mangia lo zucchero, ma non vuol purgarsi: Però quando vede i becchini che vengono a portarlo via, allora si purga. Poi dice una bugia e per gastigo gli cresce il naso.
     * XVIII Pinocchio ritrova la Volpe e il Gatto, e va con loro a seminare le quattro monete nel Campo de’ Miracoli.
     * XIX Pinocchio è derubato delle sue monete d’oro e, per gastigo, si busca quattro mesi di prigione.
     * XX Liberato dalla prigione, si avvia per tornare a casa della Fata; ma lungo la strada trova un serpente orribile, e poi rimane preso alla tagliuola.
     * XXI Pinocchio è preso da un contadino, il quale lo costringe a far da can da guardia a un pollaio.
     * XXII Pinocchio scuopre i ladri e, in ricompensa di essere stato fedele, vien posto in libertà.
     * XXIII Pinocchio piange la morte della bella Bambina dai capelli turchini: poi trova un Colombo che lo porta sulla riva del mare, e lì si getta nell’acqua per andare in aiuto del suo babbo Geppetto.
     * XXIV Pinocchio arriva all’isola delle Api industriose e ritrova la Fata.
     * XXV Pinocchio promette alla Fata di essere buono e di studiare, perché è stufo di fare il burattino e vuol diventare un bravo ragazzo.
     * XXVI Pinocchio va co’ suoi compagni di scuola in riva al mare, per vedere il terribile Pescecane.
     * XXVII Gran combattimento fra Pinocchio e i suoi compagni: uno de’ quali essendo rimasto ferito, Pinocchio viene arrestato dai carabinieri.
     * XXVIII Pinocchio corre pericolo di essere fritto in padella come un pesce.
     * XXIX Ritorna a casa della Fata, la quale gli promette che il giorno dopo non sarà più un burattino, ma diventerà un ragazzo. Gran colazione di caffè-e-latte per festeggiare questo grande avvenimento.
     * XXX Pinocchio, invece di diventare un ragazzo, parte di nascosto col suo amico Lucignolo per il Paese dei Balocchi.
     * XXXI Dopo cinque mesi di cuccagna, Pinocchio, con sua grande maraviglia, sente spuntarsi un bel paio d’orecchie asinine e diventa un ciuchino, con la coda e tutto.
     * XXXII A Pinocchio gli vengono gli orecchi di ciuco, e poi diventa un ciuchino vero e comincia a ragliare.
     * XXXIII Diventato un ciuchino vero, è portato a vendere, e lo compra il direttore di una compagnia di pagliacci per insegnargli a ballare e a saltare i cerchi; ma una sera azzoppisce e allora lo ricompra un altro, per far con la sua pelle un tamburo.
     * XXXIV Pinocchio, gettato in mare, è mangiato dai pesci e ritorna ad essere un burattino come prima; ma mentre nuota per salvarsi, è ingoiato dal terribile Pesce-cane.
     * XXXV Pinocchio ritrova in corpo al Pesce-cane... Chi ritrova? Leggete questo capitolo e lo saprete.
     * XXXVI Finalmente Pinocchio cessa d’essere un burattino e diventa un ragazzo.
     *
     * @see http://www.liberliber.it/libri/c/collodi/index.php#elenco_opere
     * @var string
     */
    protected static $baseText = <<<'EOT'
I. Come andò che maestro Ciliegia, falegname, trovò un pezzo di legno, che piangeva e rideva come un bambino.

C’era una volta...
– Un re! – diranno subito i miei piccoli lettori.
No, ragazzi, avete sbagliato. C’era una volta un pezzo di legno.
Non era un legno di lusso, ma un semplice pezzo da catasta, di quelli che d’inverno si mettono nelle stufe e nei caminetti per accendere il fuoco e per riscaldare le stanze.
Non so come andasse, ma il fatto gli è che un bel giorno questo pezzo di legno capitò nella bottega di un vecchio falegname, il quale aveva nome mastr’Antonio, se non che tutti lo chiamavano maestro Ciliegia, per via della punta del suo naso, che era sempre lustra e paonazza, come una ciliegia matura.
Appena maestro Ciliegia ebbe visto quel pezzo di legno, si rallegrò tutto e dandosi una fregatina di mani per la contentezza, borbottò a mezza voce:
– Questo legno è capitato a tempo: voglio servirmene per fare una gamba di tavolino.
Detto fatto, prese subito l’ascia arrotata per cominciare a levargli la scorza e a digrossarlo, ma quando fu lì per lasciare andare la prima asciata, rimase col braccio sospeso in aria, perché sentì una vocina sottile, che disse raccomandandosi:
– Non mi picchiar tanto forte!
Figuratevi come rimase quel buon vecchio di maestro Ciliegia!
Girò gli occhi smarriti intorno alla stanza per vedere di dove mai poteva essere uscita quella vocina, e non vide nessuno! Guardò sotto il banco, e nessuno; guardò dentro un armadio che stava sempre chiuso, e nessuno; guardò nel corbello dei trucioli e della segatura, e nessuno; apri l’uscio di bottega per dare un’occhiata anche sulla strada, e nessuno! O dunque?...
– Ho capito; – disse allora ridendo e grattandosi la parrucca, – si vede che quella vocina me la sono figurata io. Rimettiamoci a lavorare.
E ripresa l’ascia in mano, tirò giù un solennissimo colpo sul pezzo di legno.
– Ohi! tu m’hai fatto male! – gridò rammaricandosi la solita vocina.
Questa volta maestro Ciliegia restò di stucco, cogli occhi fuori del capo per la paura, colla bocca spalancata e colla lingua giù ciondoloni fino al mento, come un mascherone da fontana. Appena riebbe l’uso della parola, cominciò a dire tremando e balbettando dallo spavento:
– Ma di dove sarà uscita questa vocina che ha detto ohi?... Eppure qui non c’è anima viva. Che sia per caso questo pezzo di legno che abbia imparato a piangere e a lamentarsi come un bambino? Io non lo posso credere. Questo legno eccolo qui; è un pezzo di legno da caminetto, come tutti gli altri, e a buttarlo sul fuoco, c’è da far bollire una pentola di fagioli... O dunque? Che ci sia nascosto dentro qualcuno? Se c’è nascosto qualcuno, tanto peggio per lui. Ora l’accomodo io!
E così dicendo, agguantò con tutt’e due le mani quel povero pezzo di legno e si pose a sbatacchiarlo senza carità contro le pareti della stanza.
Poi si messe in ascolto, per sentire se c’era qualche vocina che si lamentasse. Aspettò due minuti, e nulla; cinque minuti, e nulla; dieci minuti, e nulla!
– Ho capito, – disse allora sforzandosi di ridere e arruffandosi la parrucca, – si vede che quella vocina che ha detto ohi, me la sono figurata io! Rimettiamoci a lavorare.
E perché gli era entrata addosso una gran paura, si provò a canterellare per farsi un po’ di coraggio.
Intanto, posata da una parte l’ascia, prese in mano la pialla, per piallare e tirare a pulimento il pezzo di legno; ma nel mentre che lo piallava in su e in giù, senti la solita vocina che gli disse ridendo:
– Smetti! tu mi fai il pizzicorino sul corpo!
Questa volta il povero maestro Ciliegia cadde giù come fulminato. Quando riaprì gli occhi, si trovò seduto per terra.
Il suo viso pareva trasfigurato, e perfino la punta del naso, di paonazza come era quasi sempre, gli era diventata turchina dalla gran paura.

II. Maestro Ciliegia regala il pezzo di legno al suo amico Geppetto, il quale lo prende per fabbricarsi un burattino maraviglioso che sappia ballare, tirar di scherma e fare i salti mortali.

In quel punto fu bussato alla porta.
– Passate pure, – disse il falegname, senza aver la forza di rizzarsi in piedi.
Allora entrò in bottega un vecchietto tutto arzillo, il quale aveva nome Geppetto; ma i ragazzi del vicinato, quando lo volevano far montare su tutte le furie, lo chiamavano col soprannome di Polendina, a motivo della sua parrucca gialla che somigliava moltissimo alla polendina di granturco.
Geppetto era bizzosissimo. Guai a chiamarlo Polendina! Diventava subito una bestia e non c’era più verso di tenerlo.
– Buon giorno, mastr’Antonio, – disse Geppetto. – Che cosa fate costì per terra?
– Insegno l’abbaco alle formicole.
– Buon pro vi faccia!
– Chi vi ha portato da me, compar Geppetto?
– Le gambe. Sappiate, mastr’Antonio, che son venuto da voi, per chiedervi un favore.
– Eccomi qui, pronto a servirvi, – replicò il falegname, rizzandosi su i ginocchi.
– Stamani m’è piovuta nel cervello un’idea.
– Sentiamola.
– Ho pensato di fabbricarmi da me un bel burattino di legno; ma un burattino maraviglioso, che sappia ballare, tirare di scherma e fare i salti mortali. Con questo burattino voglio girare il mondo, per buscarmi un tozzo di pane e un bicchier di vino; che ve ne pare?
– Bravo Polendina! – gridò la solita vocina, che non si capiva di dove uscisse.
A sentirsi chiamar Polendina, compar Geppetto diventò rosso come un peperone dalla bizza, e voltandosi verso il falegname, gli disse imbestialito:
– Perché mi offendete?
– Chi vi offende?
– Mi avete detto Polendina!...
– Non sono stato io.
– Sta’ un po’ a vedere che sarò stato io! Io dico che siete stato voi.
– No!
– Si!
– No!
– Si!
E riscaldandosi sempre più, vennero dalle parole ai fatti, e acciuffatisi fra di loro, si graffiarono, si morsero e si sbertucciarono.
Finito il combattimento, mastr’Antonio si trovò fra le mani la parrucca gialla di Geppetto, e Geppetto si accorse di avere in bocca la parrucca brizzolata del falegname.
– Rendimi la mia parrucca! – gridò mastr’Antonio.
– E tu rendimi la mia, e rifacciamo la pace.
I due vecchietti, dopo aver ripreso ognuno di loro la propria parrucca, si strinsero la mano e giurarono di rimanere buoni amici per tutta la vita.
– Dunque, compar Geppetto, – disse il falegname in segno di pace fatta, – qual è il piacere che volete da me?
– Vorrei un po’ di legno per fabbricare il mio burattino; me lo date?
Mastr’Antonio, tutto contento, andò subito a prendere sul banco quel pezzo di legno che era stato cagione a lui di tante paure. Ma quando fu lì per consegnarlo all’amico, il pezzo di legno dette uno scossone e sgusciandogli violentemente dalle mani, andò a battere con forza negli stinchi impresciuttiti del povero Geppetto.
– Ah! gli è con questo bel garbo, mastr’Antonio, che voi regalate la vostra roba? M’avete quasi azzoppito!...
– Vi giuro che non sono stato io!
– Allora sarò stato io!...
– La colpa è tutta di questo legno...
– Lo so che è del legno: ma siete voi che me l’avete tirato nelle gambe!
– Io non ve l’ho tirato!
– Bugiardo!
– Geppetto, non mi offendete; se no vi chiamo Polendina!...
– Asino!
– Polendina!
– Somaro!
– Polendina!
– Brutto scimmiotto!
– Polendina!
A sentirsi chiamar Polendina per la terza volta, Geppetto perse il lume degli occhi, si avvento sul falegname; e lì se ne dettero un sacco e una sporta.
A battaglia finita, mastr’Antonio si trovo due graffi di più sul naso, e quell’altro due bottoni di meno al giubbetto. Pareggiati in questo modo i loro conti, si strinsero la mano e giurarono di rimanere buoni amici per tutta la vita.
Intanto Geppetto prese con se il suo bravo pezzo di legno, e ringraziato mastr’Antonio, se ne tornò zoppicando a casa.

III Geppetto, tornato a casa, comincia subito a fabbricarsi il burattino e gli mette il nome di Pinocchio. Prime monellerie del burattino.

La casa di Geppetto era una stanzina terrena, che pigliava luce da un sottoscala. La mobilia non poteva essere più semplice: una seggiola cattiva, un letto poco buono e un tavolino tutto rovinato. Nella parete di fondo si vedeva un caminetto col fuoco acceso; ma il fuoco era dipinto, e accanto al fuoco c’era dipinta una pentola che bolliva allegramente e mandava fuori una nuvola di fumo, che pareva fumo davvero.
Appena entrato in casa, Geppetto prese subito gli arnesi e si pose a intagliare e a fabbricare il suo burattino.
– Che nome gli metterò? – disse fra sé e sé. – Lo voglio chiamar Pinocchio. Questo nome gli porterà fortuna. Ho conosciuto una famiglia intera di Pinocchi: Pinocchio il padre, Pinocchia la madre e Pinocchi i ragazzi, e tutti se la passavano bene. Il più ricco di loro chiedeva l’elemosina.
Quando ebbe trovato il nome al suo burattino, allora cominciò a lavorare a buono, e gli fece subito i capelli, poi la fronte, poi gli occhi.
Fatti gli occhi, figuratevi la sua maraviglia quando si accorse che gli occhi si muovevano e che lo guardavano fisso fisso.
Geppetto, vedendosi guardare da quei due occhi di legno, se n’ebbe quasi per male, e disse con accento risentito:
– Occhiacci di legno, perché mi guardate?
Nessuno rispose.
Allora, dopo gli occhi, gli fece il naso; ma il naso, appena fatto, cominciò a crescere: e cresci, cresci, cresci diventò in pochi minuti un nasone che non finiva mai.
Il povero Geppetto si affaticava a ritagliarlo; ma più lo ritagliava e lo scorciva, e più quel naso impertinente diventava lungo.
Dopo il naso, gli fece la bocca.
La bocca non era ancora finita di fare, che cominciò subito a ridere e a canzonarlo.
– Smetti di ridere! – disse Geppetto impermalito; ma fu come dire al muro.
– Smetti di ridere, ti ripeto! – urlò con voce minacciosa.
Allora la bocca smesse di ridere, ma cacciò fuori tutta la lingua.
Geppetto, per non guastare i fatti suoi, finse di non avvedersene, e continuò a lavorare.
Dopo la bocca, gli fece il mento, poi il collo, le spalle, lo stomaco, le braccia e le mani.
Appena finite le mani, Geppetto senti portarsi via la parrucca dal capo. Si voltò in su, e che cosa vide? Vide la sua parrucca gialla in mano del burattino.
– Pinocchio!... rendimi subito la mia parrucca!
E Pinocchio, invece di rendergli la parrucca, se la messe in capo per sé, rimanendovi sotto mezzo affogato.
A quel garbo insolente e derisorio, Geppetto si fece triste e melanconico, come non era stato mai in vita sua, e voltandosi verso Pinocchio, gli disse:
– Birba d’un figliuolo! Non sei ancora finito di fare, e già cominci a mancar di rispetto a tuo padre! Male, ragazzo mio, male!
E si rasciugò una lacrima.
Restavano sempre da fare le gambe e i piedi.
Quando Geppetto ebbe finito di fargli i piedi, sentì arrivarsi un calcio sulla punta del naso.
– Me lo merito! – disse allora fra sé. – Dovevo pensarci prima! Ormai è tardi!
Poi prese il burattino sotto le braccia e lo posò in terra, sul pavimento della stanza, per farlo camminare.
Pinocchio aveva le gambe aggranchite e non sapeva muoversi, e Geppetto lo conduceva per la mano per insegnargli a mettere un passo dietro l’altro.
Quando le gambe gli si furono sgranchite, Pinocchio cominciò a camminare da sé e a correre per la stanza; finché, infilata la porta di casa, saltò nella strada e si dette a scappare.
E il povero Geppetto a corrergli dietro senza poterlo raggiungere, perché quel birichino di Pinocchio andava a salti come una lepre, e battendo i suoi piedi di legno sul lastrico della strada, faceva un fracasso, come venti paia di zoccoli da contadini.
– Piglialo! piglialo! – urlava Geppetto; ma la gente che era per la via, vedendo questo burattino di legno, che correva come un barbero, si fermava incantata a guardarlo, e rideva, rideva e rideva, da non poterselo figurare.
Alla fine, e per buona fortuna, capitò un carabiniere, il quale, sentendo tutto quello schiamazzo e credendo si trattasse di un puledro che avesse levata la mano al padrone, si piantò coraggiosamente a gambe larghe in mezzo alla strada, coll’animo risoluto di fermarlo e di impedire il caso di maggiori disgrazie.
Ma Pinocchio, quando si avvide da lontano del carabiniere che barricava tutta la strada, s’ingegnò di passargli, per sorpresa, frammezzo alle gambe, e invece fece fiasco.
Il carabiniere, senza punto smoversi, lo acciuffò pulitamente per il naso (era un nasone spropositato, che pareva fatto apposta per essere acchiappato dai carabinieri), e lo riconsegnò nelle proprie mani di Geppetto; il quale, a titolo di correzione, voleva dargli subito una buona tiratina d’orecchi. Ma figuratevi come rimase quando, nel cercargli gli orecchi, non gli riuscì di poterli trovare: e sapete perché? Perché, nella furia di scolpirlo, si era dimenticato di farglieli.
Allora lo prese per la collottola, e, mentre lo riconduceva indietro, gli disse tentennando minacciosamente il capo:
– Andiamo a casa. Quando saremo a casa, non dubitare che faremo i nostri conti!
Pinocchio, a questa antifona, si buttò per terra, e non volle più camminare. Intanto i curiosi e i bighelloni principiavano a fermarsi lì dintorno e a far capannello.
Chi ne diceva una, chi un’altra.
– Povero burattino! – dicevano alcuni, – ha ragione a non voler tornare a casa! Chi lo sa come lo picchierebbe quell’omaccio di Geppetto!...
E gli altri soggiungevano malignamente:
– Quel Geppetto pare un galantuomo! ma è un vero tiranno coi ragazzi! Se gli lasciano quel povero burattino fra le mani, è capacissimo di farlo a pezzi!...
Insomma, tanto dissero e tanto fecero, che il carabiniere rimise in libertà Pinocchio e condusse in prigione quel pover’uomo di Geppetto. Il quale, non avendo parole lì per lì per difendersi, piangeva come un vitellino, e nell’avviarsi verso il carcere, balbettava singhiozzando:
– Sciagurato figliuolo! E pensare che ho penato tanto a farlo un burattino per bene! Ma mi sta il dovere! Dovevo pensarci prima!...
Quello che accadde dopo, è una storia da non potersi credere, e ve la racconterò in quest’altri capitoli.

IV La storia di Pinocchio col Grillo-parlante, dove si vede come i ragazzi cattivi hanno a noia di sentirsi correggere da chi ne sa più di loro.

Vi dirò dunque, ragazzi, che mentre il povero Geppetto era condotto senza sua colpa in prigione, quel monello di Pinocchio, rimasto libero dalle grinfie del carabiniere, se la dava a gambe giù attraverso ai campi, per far più presto a tornarsene a casa; e nella gran furia del correre saltava greppi altissimi, siepi di pruni e fossi pieni d’acqua, tale e quale come avrebbe potuto fare un capretto o un leprottino inseguito dai cacciatori.
Giunto dinanzi a casa, trovò l’uscio di strada socchiuso. Lo spinse, entrò dentro, e appena ebbe messo tanto di paletto, si gettò a sedere per terra, lasciando andare un gran sospirone di contentezza.
Ma quella contentezz