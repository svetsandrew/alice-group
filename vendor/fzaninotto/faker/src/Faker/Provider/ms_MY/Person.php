<?php
namespace Faker\Provider\ms_MY;

use Faker\Provider\DateTime;
use Faker\Generator;

class Person extends \Faker\Provider\Person
{
    protected static $firstNameFormat = array(
        '{{firstNameMaleMalay}}',
        '{{firstNameFemaleMalay}}',
        '{{firstNameMaleChinese}}',
        '{{firstNameFemaleChinese}}',
        '{{firstNameMaleIndian}}',
        '{{firstNameFemaleIndian}}',
        '{{firstNameMaleChristian}}',
        '{{firstNameFemaleChristian}}',
    );

    /**
     * @link https://en.wikipedia.org/wiki/Malaysian_names
     */
    protected static $maleNameFormats = array(
        //Malay
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}} bin {{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}} bin {{titleMaleMalay}}{{firstNameMaleMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}} bin {{titleMaleMalay}}{{lastNameMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} bin {{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} bin {{titleMaleMalay}}{{firstNameMaleMalay}}',
        '{{muhammadName}}{{haji}}{{titleMaleMalay}}{{firstNameMaleMalay}} bin {{titleMaleMalay}}{{lastNameMalay}}',
        //Chinese
        '{{lastNameChinese}} {{firstNameMaleChinese}}',
        '{{lastNameChinese}} {{firstNameMaleChinese}}',
        '{{lastNameChinese}} {{firstNameMaleChinese}}',
        '{{lastNameChinese}} {{firstNameMaleChinese}}',
        '{{lastNameChinese}} {{firstNameMaleChinese}}',
        '{{firstNameMaleChristian}} {{lastNameChinese}} {{firstNameMaleChinese}}',
        //Indian
        '{{initialIndian}} {{firstNameMaleIndian}}',
        '{{initialIndian}} {{lastNameIndian}}',
        '{{firstNameMaleIndian}} a/l {{firstNameMaleIndian}}',
        '{{firstNameMaleIndian}} a/l {{firstNameMaleIndian}} {{lastNameIndian}}',
        '{{firstNameMaleIndian}} {{lastNameIndian}} a/l {{lastNameIndian}}',
        '{{firstNameMaleIndian}} {{lastNameIndian}} a/l {{firstNameMaleIndian}} {{lastNameIndian}}',
        '{{firstNameMaleIndian}} {{lastNameIndian}}',
    );

    /**
     * @link https://en.wikipedia.org/wiki/Malaysian_names
     */
    protected static $femaleNameFormats = array(
        //Malay
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} {{lastNameMalay}} binti {{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} {{lastNameMalay}} binti {{titleMaleMalay}}{{firstNameMaleMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} {{lastNameMalay}} binti {{titleMaleMalay}}{{lastNameMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} {{lastNameMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} binti {{titleMaleMalay}}{{firstNameMaleMalay}} {{lastNameMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} binti {{titleMaleMalay}}{{firstNameMaleMalay}}',
        '{{nurName}}{{hajjah}}{{firstNameFemaleMalay}} binti {{titleMaleMalay}}{{lastNameMalay}}',
        //Chinese
        '{{lastNameChinese}} {{firstNameFemaleChinese}}',
        '{{lastNameChinese}} {{firstNameFemaleChinese}}',
        '{{lastNameChinese}} {{firstNameFemaleChinese}}',
        '{{lastNameChinese}} {{firstNameFemaleChinese}}',
        '{{lastNameChinese}} {{firstNameFemaleChinese}}',
        '{{firstNameFemaleChristian}} {{lastNameChinese}} {{firstNameFemaleChinese}}',
        //Indian
        '{{initialIndian}}{{firstNameFemaleIndian}}',
        '{{initialIndian}}{{lastNameIndian}}',
        '{{firstNameFemaleIndian}} a/l {{firstNameMaleIndian}}',
        '{{firstNameFemaleIndian}} a/l {{firstNameMaleIndian}} {{lastNameIndian}}',
        '{{firstNameFemaleIndian}} {{lastNameIndian}} a/l {{firstNameMaleIndian}}',
        '{{firstNameFemaleIndian}} {{lastNameIndian}} a/l {{firstNameMaleIndian}} {{lastNameIndian}}',
        '{{firstNameFemaleIndian}} {{lastNameIndian}}',
    );

    /**
     * @link https://en.wikipedia.org/wiki/List_of_Malay_people
     * @link https://samttar.edu.my/senarai-nama-pelajar-2016/
     * @link http://smkspkl.edu.my/senarai-nama-pelajar
     */
    protected static $firstNameMaleMalay = array(
        'A','A.r','A\'fif','A\'zizul','Ab','Abadi','Abas','Abd','Abd.','Abd.rahim','Abdel','Abdul','Abdull','Abdullah','Abdulloh','Abu','Adam','Adi','Adib','Adil','Adnan','Ady','Adzmin','Afandy','Afif','Afiq','Afza','Agus','Ahmad','Ahmat','Ahmed','Ahwali','Ahyer','Aidid','Aidil','Aiman','Aimman','Ainol','Ainuddin','Ainul','Aizad','Aizam','Aizat','Aizuddin','Ajis','Ajmal','Ajwad','Akhmal','Akid','Akif','Akmal','Al','Al-afnan','Al-muazrar','Alfian','Ali','Alias','Alif','Aliff','Alilah','Alin','Allif','Amaanullah','Amami','Aman','Amar','Ameershah','Amier','Amierul','Amil','Amin','Aminuddin','Amir','Amiruddin','Amirul','Ammar','Amran','Amri','Amru','Amrullah','Amsyar','Anas','Andri','Aniq','Anuar','Anuwar','Anwar','Aqeel','Aqif','Aqil','Arash','Arbani','Arefin','Arief','Arif','Arifen','Ariff','Ariffin','Arifin','Armi','Ashraf','Ashraff','Ashrof','Ashrul','Aslam','Asmawi','Asmin','Asmuri','Asraf','Asri','Asrialif','Asror','Asrul','Asymawi','Asyraaf','Asyraf','Atan','Athari','Awaludin','Awira','Azam','Azely','Azfar','Azhan','Azhar','Azhari','Azib','Azim','Aziz','Azizan','Azizul','Azizulhasni','Azlan','Azlee','Azli','Azman','Azmi','Azmie','Azmin','Aznan','Aznizam','Azraai','Azri','Azrie','Azrien','Azril','Azrin','Azrul','Azry','Azuan',
        'Badri','Badrullesham','Baharin','Baharuddin','Bahrul','Bakri','Basaruddin','Basiran','Basirin','Basri','Basyir','Bazli','Borhan','Buang','Budi','Bukhari','Bukharudin','Bustaman','Buyung',
        'Chailan',
        'Dahalan','Dailami','Dan','Danial','Danie','Daniel','Danien','Danish','Darimin','Darul','Darus','Darwisy','Dhiyaulhaq','Diah','Djuhandie','Dolbahrin','Dolkefli','Dzikri','Dzul','Dzulfahmi','Dzulfikri','Dzulkarnaen',
        'Eazriq','Effendi','Ehza','Eizkandar','Ekhsan','Elyas','Enidzullah','Ezam','Ezani',
        'Fadhil','Fadly','Fadzil','Fadziruddin','Fadzli','Fahmi','Faiq','Fairuz','Faisal','Faiz','Faizal','Faizurrahman','Fakhrul','Fakhrullah','Farham','Farhan','Farid','Faris','Farisan','Fariz','Fasil','Fateh','Fathi','Fathuddin','Fathul','Fauzan','Fauzi','Fauzul','Fawwaz','Fazal','Fazly','Fazreen','Fazril','Fendi','Fikri','Fikrie','Fikrul','Firdaus','Fithri','Fitiri','Fitri','Fuad',
        'Ghazali',
        'Habib','Haddad','Hadi','Hadif','Hadzir','Haffize','Haffizi','Hafidzuddin','Hafis','Hafiy','Hafiz','Hafizan','Hafizhan','Hafizi','Hafizsyakirin','Hafizuddin','Haikal','Haiqal','Hairol','Hairollkahar','Hairuddin','Hairul','Hairun','Haisyraf','Haizan','Hakeem','Hakim','Hakimi','Hakimie','Halidan','Haliem','Halim','Hamdani','Hamidoon','Hamizan','Hamka','Hamzah','Hanafi','Hanif','Hanit','Hannan','Haqeem','Haqimie','Harez','Haris','Harith','Hariz','Harmaini','Harraz','Harun','Hasan','Hashim','Hasif','Hasnul','Hasrin','Hasrol','Hassan','Hasyim','Haszlan','Hayani','Hazim','Haziq','Haziqh','Hazrie','Hazrul','Hazwan','Hazzam','Helmy','Hermansah','Hidayat','Hidayatullah','Hilmi','Hisam','Hisammudin','Hisyam','Hj','Hoirussalam','Humadu','Hurmin','Husain','Husaini','Husnul','Hussein','Hussin','Huzaifi','Huzaimi','Huzzaini',
        'Ibnu','Ibrahim','Idham','Idlan','Idris','Idrus','Idzwan','Ielman','Ighfar','Ihsan','Ikhmal','Ikhwan','Ikmal','Ilham','Ilhan','Illias','Ilman','Iman','Imran','Indra','Innamul','Iqbal','Iqwan','Iraman','Irfan','Irman','Irsyad','Isa','Ishak','Ishaq','Iskandar','Isma','Ismail','Ismaon','Isyraq','Iwan','Iyad','Izam','Izdihar','Izlan','Izuhail','Izwan','Izz','Izzan','Izzat','Izzikry','Izzuddin','Izzul',
        'Ja\'afer','Jaf','Jaferi','Jafree','Jafri','Jahari','Jalani','Jamal','Jamali','Jamalludin','Jamaluddin','Jamekon','Jamil','Jamsare','Jani','Jasin','Jasni','Jebat','Jefrie','Johari','Joharudin','Jumat','Junaidi',
        'Kamal','Kamaruddin','Kamarudin','Kamarul','Kamaruzain','Kamaruzaman','Kamaruzzaman','Kasim','Kasturi','Kemat','Khadzromi','Khairi','Khairil','Khairin','Khairiz','Khairol','Khairubi','Khairudin','Khairul','Khairulnizam','Khairun','Khairurrazi','Khalilul','Khasmadi','Khasri','Khatta','Khirul','Khoirul','Kholis','Khusaini','Khuzairey','Kutni',
        'Latiff','Lazim','Lokman','Loqman','Lufty','Lukman','Luqman','Luqmanul','Luthfi','Luthfie',
        'M.','Maamor','Madfaizal','Mahadhir','Mahatdir','Mahmusin','Mansor','Marlizam','Martonis','Mastura','Mat','Mazlan','Mazmin','Mazwan','Md','Md.','Megat','Meor','Midoon','Mie','Mikhail','Mirza','Misbun','Miskan','Misran','Miza','Mohlim','Mohmad','Mokhtar','Mokhzani','Moktar','Mu\'izzuddin','Muazzam','Mubarak','Muhaimen','Muhaimi','Muhammad','Muhd','Muid','Muizzuddin','Muizzudin','Mukhtar','Mukhriz','Mukminin','Murad','Murshid','Mus\'ab','Musa','Musiran','Muslim','Mustafa','Mustain','Mustaqim','Musyrif','Muszaphar','Muzami','Muzamil','Muzhafar','Muzzammil',
        'Na\'imullah','Nabil','Naderi','Nadzeri','Naim','Najhan','Najib','Najmi','Nakimie','Naqib','Naqiuddin','Narul','Nasaruddin','Nashrul','Nasimuddin','Nasir','Nasiruddin','Nasri','Nasrizal','Nasruddin','Nasrul','Nasrullah','Naufal','Nawawi','Nazari','Nazaruddin','Nazarul','Nazeem','Nazeri','Nazhan','Nazim','Nazlan','Nazmi','Nazren','Nazri','Nazril','Nazrin','Nazrul','Nazzab','Ngadinin','Ngasiman','Ngatri','Nik','Nizam','Nizan','Nizar','Noor','Noordin','Noorizman','Nor','Norain','Norazman','Norazmi','Nordanish','Nordiarman','Nordin','Norfadli','Norfahmi','Norhakim','Norhan','Norhisham','Norsilan','Nur','Nur\'irfaan','Nurakmal','Nurhanafi','Nurhazrul','Nurul','Nuwair','Nuzrul','Nuzul',
        'Omar','Omri','Osama','Osman','Othman',
        'Pauzi','Puadi','Putra',
        'Qairil','Qays','Qusyairi',
        'R','Radin','Radzi','Radzuan','Rafael','Raffioddin','Rafiee','Rafiq','Rafizal','Rahim','Raihan','Raja','Rakmat','Ramdan','Ramlan','Ramli','Rash','Rashdan','Rashid','Rashidi','Rasid','Raulah','Rausyan','Razak','Razali','Razemi','Razif','Razlan','Razuan','Redzuan','Redzuawan','Redzwan','Rehan','Rehman','Rezal','Ridhuan','Ridwan','Ridza','Ridzuan','Ridzwan','Rifqi','Rizal','Rizli','Rohaizad','Rohaizal','Rohman','Roosmadi','Roseli','Roslan','Roslee','Rosli','Roslin','Rosman','Rosnan','Rossafizal','Rozi','Rukaini','Rukmanihakim','Ruknuddin','Ruslan','Rusli','Rusman',
        'S.rozli','Sabana','Sabqi','Sabri','Sadili','Sadri','Saf\'han','Saffrin','Safie','Safiy','Safrizal','Safuan','Safwan','Sahamudin','Saharil','Said','Saidan','Saidin','Saif','Saiful','Saifullah','Saifullizan','Saipol','Sakri','Salamon','Salihin','Salimi','Salleh','Samad','Samani','Sameer','Samiun','Samsul','Samsur','Sanorhizam','Sardine','Sarudin','Sarwati','Saufishazwi','Sazali','Selamat','Senon','Shafarizal','Shafie','Shafiq','Shah','Shahamirul','Shaharudin','Shaheila','Shaheizy','Shahfiq','Shahmi','Shahnon','Shahquzaifi','Shahril','Shahrin','Shahrizal','Shahrol','Shahru','Shahrul','Shahrulnaim','Shahrun','Shahrunizam','Shahzwan','Shaiful','Shaikh','Shakif','Shakir','Sham','Shameer','Shamhazli','Shamil','Shamizan','Shamizul','Shamsuddin','Shamsudin','Shamsul','Shamsuri','Shamsuzlynn','Shapiein','Sharafuddin','Shari','Sharif','Sharifuddin','Sharifudin','Sharil','Sharizal','Sharsham','Sharudin','Sharul','Shaugi','Shauqi','Shawal','Shazwan','Sheikh','Shmsul','Shohaimi','Shukri','Sirajuddin','Sofian','Sohaini','Solehen','Solekhan','Solleh','Sualman','Subbahi','Subkhiddin','Sudarrahman','Sudirman','Suhaimi','Sukarni','Sukhairi','Sukri','Sukymi','Sulaiman','Sulhan','Suzaili','Suzaman','Syafiq','Syahaziq','Syahid','Syahir','Syahmi','Syahrial','Syahriman','Syahru','Syahzuan','Syakir','Syakirin','Syakirul','Syamirul','Syamsol','Syaqirin','Syarafuddin','Syawal','Syawalludin','Syazani','Syazwan','Syed','Syid','Syukri','Syuqeri',
        'Tajuddin','Takiudin','Talha','Tarmizi','Tasripin','Taufek','Taufik','Tayib','Termizi','Thalahuddin','Thaqif','Tunan',
        'Umair','Umar','Usman',
        'W','Wafi','Wafiq','Wan','Wazir','Wazzirul','Wi',
        'Yani','Yaqzan','Yazid','Yunos','Yusaini','Yusfaisal','Yushafiq','Yusni','Yusof','Yusoff','Yusri','Yussof','Yusuf',
        'Zabayudin','Zabidi','Zahari','Zahid','Zahiruddin','Zahrul','Zaid','Zaidi','Zainal','Zaini','Zainodin','Zainordin','Zainuddin','Zainul','Zairy','Zaiyon','Zakaria','Zaki','Zakii','Zakri','Zakwan','Zambri','Zamre','Zamri','Zamrul','Zan','Zaqiyuddin','Zar\'ai','Zarif','Zariq','Zarith','Zarul','Zaukepli','Zawawi','Zharaubi','Zikri','Zikril','Zikry','Zizi','Zol','Zolkifle','Zubair','Zubir','Zufayri','Zufrie','Zuheeryrizal','Zuhri','Zuki','Zul','Zulfadhli','Zulfadli','Zulfahmi','Zulfaqar','Zulfaqqar','Zulfikar','Zulhaikal','Zulhakim','Zulhakimi','Zulhelmi','Zulhilmi','Zulkapli','Zulkarnain','Zulkefli','Zulkfli','Zulkifli','Zulkipli','Zulman','Zuri'
    );
    protected static $firstNameFemaleMalay = array(
        '\'Abidah','\'Alyaa','\'Aqilah','\'Atiqah','\'Afiqah','\'Alia','\'Aqilah','A\'ishah','A\'in','A\'zizah','Abdah','Abiatul','Adani','Adawiyah','Adha','Adharina','Adhwa','Adibah','Adilah','Adilla','Adina','Adini','Adira','Adlina','Adlyna','Adriana','Adzlyana','Afifa','Afifah','Afina','Afiqah','Afiza','Afrina','Afzan','Ahda','Aida','Aidatul','Aidila','Aifa','Aiman','Aimi','Aimuni','Ain','Aina','Ainaa','Ainaanasuha','Aini','Ainin','Ainn','Ainnaziha','Ainul','Ainun','Ainur','Airin','Aishah','Aisya','Aisyah','Aiza','Akmal','Aleeya','Aleeza','Aleya','Aleza','Alia','Aliaa','Aliah','Aliffa','Aliffatus','Alina','Alis','Alisya','Aliya','Alkubra','Alleisya','Ally','Alya','Alyaa','Amalia','Amalien','Amalin','Amalina','Amani','Amanina','Amiera','Aminy','Amira','Amirah','Amisha','Amrina','Amylia','Amyra','An-nur','Anas','Andani','Andi','Anesha','Ani','Aninafishah','Anis','Anisah','Anisha','Anissa','Aniza','Anna','Anne','Antaza','Aqeem','Aqeera','Aqila','Aqilah','Arfahrina','Ariana','Ariena','Ariessa','Arifah','Arina','Ariqah','Arissa','Arisya','Armira','Arwina','Aryani','Ashika','Ashriyana','Asiah','Asma\'rauha','Asmaa\'','Asmaleana','Asniati','Asnie','Asniza','Aswana','Asy','Asyiqin','Asykin','Athirah','Atifa','Atifah','Atifahajar','Atikah','Atiqa','Atiqah','Atirah','Atyqah','Auni','Awatif','Awatiff','Ayesha','Ayu','Ayuni','Ayunie','Az','Azashahira','Aziah','Aziemah','Azika','Azira','Azizah','Azliah','Azliatul','Azlin','Azlina','Azmina','Azni','Azrah','Azrina','Azua','Azuin','Azwa','Azwani','Azyan','Azyyati',
        'Badrina','Bahirah','Balqis','Basyirah','Batrisya','Batrisyia','Bilqis','Bismillah',
        'Camelia','Cempaka',
        'Dalila','Dalili','Damia','Dania','Danish','Darlina','Darwisyah','Deni','Dhani','\'Dhiya','Diana','Dianah','Dini','Diyana','Diyanah','Dylaila',
        'Eizzah','Eliya','Ellynur','Elpiya','Elyana','Elysha','Ema','Emylia','Erika','Eva','Ezzatul',
        'Faathihah','Fadhilah','Fadzliana','Fahda','Fahimah','Fahira','Fairuz','Faizah','Faiznur','Faizyani','Fakhira','Falah','Faqihah','Fara','Faradieba','Farah','Faraheira','Farahin','Farahiyah','Farahtasha','Farha','Farhah','Farhana','Faridatul','Fariha','Farina','Farisah','Farisha','Farrah','Fartinah','Farzana','Fasehah','Fasha','Fateha','Fatehah','Fathiah','Fathiha','Fathihah','Fathimah','Fatiha','Fatihah','Fatimatul','Fatin','Fatini','Fauziah','Faza','Fazlina','Fezrina','Filza','Filzah','Firzanah','Fitrah','Fitri','Fitriah','Fizra',
        'Hadfina','Hadiyatul','Hafezah','Hafidzah','Hafieza','Hafizah','Hahizah','Hajar','Hakimah','Halimatul','Halimatussa\'diah','Halisah','Hamira','Hamizah','Hana','Hanaani','Hanani','Hani','Hanim','Hanini','Hanis','Hanisah','Hanna','Hannan','Hannani','Hanni','Hanun','Harma','Hasmalinda','Hasya','Hasyimah','Hayani','Hayati','Hayatul','Hayaty','Hazira','Hazirah','Hazmeera','Hazwani','Hazwanie','Herlina','Herliyana','Hidayah','Hidzwati','Huda','Humaira','Hureen','Husna','Husnina',
        'Ida','Iffah','Iklil','Ili','Ilyana','Iman','Imelda','Insyira','Insyirah','Intan','\'Irdhina','Irdina','\'Irdina','Irsa','Iryani','\'Isdmah','Islamiah','Isnur','Izaiti','Izati','Izatie','Izatul','Izaty','Izlin','\'Izzah','Izzah','Izzani','Izzati','Izzatul','Izzaty','Izziani',
        'Jaf','Jajuenne','Jani','Jannah','Jannatul','Jaslina','Jihan','Ju','Julia','Juliana','Juliya',
        'Kamarlia','Kamelia','Kausthar','Kauthar','Khadijah','Khahirah','Khairina','Khairun','Khairunisa','Khairunnisa','Khairunnisak','Khaleeda','Khaleisya','Khaliesah','Khalisa','Khodijah',
        'Laila','Liana','Lina','Lisa','Liyana',
        'Madihah','Maheran','Mahfuzah','Mahirah','Maisara','Maisarah','Maizatul','Malihah','Mardhiah','Mariam','Marina','Mariska','Marlina','Marni','Maryam','Mas','Mashitah','Masitah','Mastura','Maswah','Masyikah','Masyitah','Maszlina','Mawaddah','Maya','Mazdiyana','Mazlyn','Melisa','Melissa','Mimi','Mira','Mirsha','Miskon','Miza','Muazzah','Mumtaz','Mursyidah','Muti\'ah','Muyassarah','Muzainah','Mysara','Mysarah',
        'Nabihah','Nabila','Nabilah','Nabilla','Nabillah','Nadhilah','Nadhirah','Nadhrah','Nadia','Nadiah','Nadiatun','Nadilla','Nadira','Nadirah','Nadwah','Nadzirah','Nafisah','Nafizah','Najah','Najian','Najiha','Najihah','Najla','Najwa','Najwani','Naliny','Naqibahuda','Nashrah','Nashuha','Nasliha','Nasrin','Nasuha','Natasa','Natasha','Natasya','Nathasa','Natrah','Naurah','Nayli','Nazatul','Nazihah','Nazira','Nazirah','Nazura','Nazurah','Nikmah','Nina','Nisa','Nisak','Nisrina','Noorain','Noorazmiera','Noorfarzanah','Noornazratul','Norafizah','Norain','Noraisyah','Noralia','Noranisa','Noratasha','Nordhiya','Nordiana','Norelliana','Norerina','Norfaezah','Norfahanna','Norhafiza','Norhamiza','Norhidayah','Noridayu','Norliyana','Norsakinah','Norshaera','Norshahirah','Norshuhailah','Norsolehah','Norsuhana','Norsyafiqah','Norsyahirah','Norsyamimie','Norsyarah','Norsyazmira','Norsyazwani','Norsyuhada','Norul','Noryshah',
        'Nuradilah','Nurafifah','Nurafrina','Nurain','Nuraina','Nuralia','Nuraliah','Nuralifah','Nuralya','Nurani','Nuranisya','Nuraqilah','Nurarisha','Nurasyikin','Nuratiqah','Nuraveena','Nureen','Nurfaatihah','Nurfadlhlin','Nurfaizah','Nurfarah','Nurfarahin','Nurfarhana','Nurfarrah','Nurfatehah','Nurfatiha','Nurfatin','Nurfirzanah','Nurfitrah','Nurfizatul','Nurhafizah','Nurhajar','Nurhani','Nurhanida','Nurhanis','Nurhanisah','Nurhanna','Nurhawa','Nurhazwani','Nurhazzimah','Nurhidayah','Nurhidayatul','Nurhuda','Nurilyani','Nurin','Nurjazriena','Nurmuzdalifah','Nurnajiha','Nurnatasha','Nurnazhimah','Nurnazhirah','Nurqurratuain','Nursabrina','Nursahira','Nursarah','Nursarwindah','Nursham','Nurshammeza','Nursofiah','Nursuhaila','Nursyaffira','Nursyafika','Nursyahindah','Nursyakirah','Nursyarina','Nursyazwani','Nursyazwina','Nursyuhadah','Nurulhuda','Nurulsyahida','Nurun','Nurwadiyah','Nurwahidah','Nurzafira','Nurzarith','Nurzulaika',
        'Pesona','Puteri','Putri',
        'Qairina','Qamarina','Qasrina','Qhistina','Qistina','Quintasya','Qurratu','Qurratuaini','Qurratul',
        'Rabi\'atul','Rabiatul','Rafidah','Rahiemah','Rahmah','Raihah','Raihana','Raihanah','Raja','Rashmi','Rasyaratul','Rasyiqah','Rasyiqqah','Raudatul','Ridiatul','Rieni','Rifhan','Rihhadatul','Ros','Rosalinda','Rosyadah','Rusyda','Rusydina',
        'Sa\'adah','Saadiah','Sabrina','Safi','Safiah','Safiyah','Sahira','Saidatul','Sakinah','Sakirah','Salwa','Sameera','Sarah','Sarwati','Sasya','Serene','Sha','Shabariah','Shafiah','Shafiera','Shafikah','Shafinaz','Shafiqa','Shafiqah','Shah','Shahida','Shahidah','Shahiera','Shahila','Shahira','Shahirah','Shahrazy','Shahrina','Shakilah','Shakinah','Shalina','Shameera','Shamila','Shamimie','Shamira','Shar\'fiera','Sharifah','Sharizah','Shauqina','Shayira','Shazana','Shazieda','Shazlien','Shazwana','Shazwani','Shonia','Shuhada','Siti','Siti','Siti','Siti','Siti','Siti','Sitti','Sofea','Sofeah','Soffia','Sofia','Sofiya','Sofiyah','Sofya','Solehah','Sopie','Suaidah','Suhada','Suhadah','Suhaida','Suhaila','Suhailah','Suhaina','Suhana','Suhani','Sulaiha','Sumayyah','Suraya','Suziyanis','Syaffea','Syafika','Syafikah','Syafina','Syafiqa','Syafiqah','Syafirah','Syafiyah','Syafiyana','Syahada','Syahadatullah','Syahera','Syaherah','Syahidah','Syahidatul','Syahiera','Syahira','Syahirah','Syahmimi','Syahmina','Syahzani','Syaidatul','Syairah','Syakila','Syakira','Syakirah','Syamien','Syamilah','Syamimi','Syamina','Syamirah','Syara','Syarafana','Syarafina','Syarah','Syarina','Syasyabila','Syauqina','Syaza','Syazana','Syazliya','Syazmin','Syazryana','Syazwana','Syazwani','Syazwanie','Syazwina','Syifa\'','Syuhada','Syuhada`','Syuhaida','Syuhaidah',
        'Taqiah','Tasnim','Tengku','Tihany',
        'Umairah','Umi','Umira','Ummi',
        'Wadiha','Wafa','Waheeda','Wahida','Wahidah','Wan','Wardatul','Wardina','Wardinah','Wazira','Weni',
        'Yasmeen','Yasmin','Yetri','Yunalis','Yusra','Yusrinaa','Yusyilaaida',
        'Zaffan','Zafirah','Zaharah','Zahirah','Zahrah','Zahrak','Zaidalina','Zaidatulkhoiriyah','Zainab','Zainatul','Zakdatul','Zatalini','Zati','Zayani','Zeqafazri','Zilhaiza','Zubaidah','Zulaika','Zulaikha'
    );
    protected static $lastNameMalay = array(
        '\'Aizat','A\'liyyuddin','Abas','Abdillah','Abdullah','Abidin','Adam','Adha','Adham','Adi','Adieka','Adip','Adli','Adnan','Adrus','Afandi','Afiq','Afizi','Afnan','Afsyal','Ahmad','Ahwali','Aidi','Aidil','Aiman','Aizad','Aizam','Aizat','Ajllin','Ajmal','Akashah','Akasyah','Akbar','Akhmal','Akid','Akif','Akmal','Al-amin','Al-hakim','Albukhary','Ali','Alias','Alif','Alimi','Aliuddin','Amaluddin','Amin','Aminnudin','Aminrullah','Aminuddin','Amiran','Amiruddin','Amirul','Amirullah','Ammar','Ammer','Amni','Amran','Amri','Amry','Amsyar','Amzah','Anam','Anaqi','Andalis','Anuar','Anwar','Apizan','Aqashah','Aqil','Arfan','Arfandi','Arias','Arief','Arif','Ariff','Ariffin','Arifin','Arifuddin','Arman','Arshad','Arziman','As','Asa','Ashraf','Ashraff','Asmadi','Asmar','Asmawi','Asri','Asyraf','Asyran','Asyrani','Aszahari','Awal','Awalluddin','Awaluddin','Awaludin','Awira','Ayyadi','Azahar','Azahari','Azam','Azhan','Azhar','Azhari','Azim','Aziz','Azizan','Azizi','Azizy','Azlan','Azlansyhah','Azli','Azlim','Azman','Azmee','Azmi','Azmin','Aznai','Azni','Azraai','Azrai','Azri','Azril','Azrin','Azriq','Azrul','Azuan',
        'Badrulhisham','Baha','Bahaman','Bahari','Baharin','Baharruddin','B